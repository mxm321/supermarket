# supermarket

#### 介绍
超市管理系统

#### 软件架构
使用多模块DAO层，Bean层，Server层、Web层（controller层）
每一层都是一个单独的工程，由maven管理依赖关系来实现相互的调用。
Dao层：主要存放负责数据库访问的，其中包含了mybatis的映射文件、dao的数据访问接口和mapper文件。
Bean层：存放实体类，类中包括定义对象以及get，set方法。
Server层：负责业务逻辑处理，其中包含业务逻辑处理接口与实现类。
Web层：主要是Controller层，主要实现前台页面与后台的交互



#### 安装教程

需要使用postman来实现数据库的查询

#### 下载网站

https://www.getpostman.com/

