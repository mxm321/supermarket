package edu.cn.service.service;

import edu.cn.Bill;
import edu.cn.dao.BillDao;
import edu.cn.service.BillIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillImplament implements BillIm {
    @Autowired
    private BillDao billIm;
    @Override
    public Bill findBill(Integer id) {
        return billIm.findBill(id);
    }

    @Override
    public int addBill(Bill bill) {
        return billIm.addBill(bill);
    }

    @Override
    public int UpdateBill(Bill bill) {
        return billIm.UpdateBill(bill);
    }

    @Override
    public int deleteBill(Integer id) {
        return billIm.deleteBill(id);
    }

    @Override
    public List<Bill> allBill() {
        List<Bill> allBill = billIm.allBill();
        return allBill;
    }
}
