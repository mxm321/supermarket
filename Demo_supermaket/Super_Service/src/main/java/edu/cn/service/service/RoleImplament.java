package edu.cn.service.service;

import edu.cn.Role;
import edu.cn.dao.RoleDao;
import edu.cn.service.RoleIm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RoleImplament implements RoleIm {
    @Autowired
    private RoleDao roleIm;

    @Override
    public Role findRole(Integer id) {
        return roleIm.findRole(id);
    }

    @Override
    public int addRole(Role role) {
        return roleIm.addRole(role);
    }

    @Override
    public int updateRole(Role role) {
        return roleIm.updateRole(role);
    }

    @Override
    public int deleteRole(Integer id) {
        return roleIm.deleteRole(id);
    }

    @Override
    public List<Role> allRole() {
        List<Role> allRole = roleIm.allRole();
        return allRole;
    }
}
