package edu.cn.service;

import edu.cn.Role;

import java.util.List;

public interface RoleIm {
    public Role findRole(Integer id);
    public int addRole(Role role);
    public int updateRole(Role role);
    public int deleteRole(Integer id);
    public List<Role> allRole();
}
