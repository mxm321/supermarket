package edu.cn.service.service;

import edu.cn.User;
import edu.cn.dao.UserDao;
import edu.cn.service.UserIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImplament implements UserIm {
    @Autowired
    private UserDao userDao;
    @Override
    public User findUser(Integer id) {
        return userDao.findUser(id);
    }

    @Override
    public int addUser(User user) {
        return userDao.addUser(user);
    }

    @Override
    public int updateUser(User user) {
        return userDao.updateUser(user);
    }
    @Override
    public int deleteUser(Integer id) {
        return userDao.deleteUser(id);
    }

    @Override
    public List<User> allUser() {
        List<User> allUser = userDao.allUser();
        return allUser;
    }
}
