package edu.cn.service.service;

import edu.cn.Address;
import edu.cn.dao.AddressDao;
import edu.cn.service.AddressIm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AddressImplement implements AddressIm {
    @Autowired
    private AddressDao addressIm;
    @Override
    public Address findAddress(Integer id) {
        return addressIm.findAddress(id);
    }
    @Override
    public int addAddress(Address address) {
        return addressIm.addAddress(address);
    }

    @Override
    public int UpdateAddress(Address address) {
        return addressIm.UpdateAddress(address);
    }

    @Override
    public int deleteAddress(Integer id) {
        return addressIm.deleteAddress(id);
    }

    @Override
    public List<Address> allAddress() {
        List<Address> allAddress = addressIm.allAddress();
        return allAddress;
    }
}
