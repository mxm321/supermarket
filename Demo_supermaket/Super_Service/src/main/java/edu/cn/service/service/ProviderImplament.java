package edu.cn.service.service;

import edu.cn.Provider;
import edu.cn.dao.ProviderDao;
import edu.cn.service.ProviderIm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProviderImplament implements ProviderIm {
    @Autowired
    private ProviderDao providerIm;
    @Override
    public Provider findProvider(Integer id) {
        return providerIm.findProvider(id);
    }

    @Override
    public int addProvider(Provider provider) {
        return providerIm.addProvider(provider);
    }

    @Override
    public int UpdateProvider(Provider provider) {
        return providerIm.UpdateProvider(provider);
    }

    @Override
    public int deleteProvider(Integer id) {
        return providerIm.deleteProvider(id);
    }

    @Override
    public List<Provider> allProvider() {
        List<Provider> allProvider = providerIm.allProvider();
        return allProvider;
    }
}
