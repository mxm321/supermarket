package edu.cn.service;

import edu.cn.User;

import java.util.List;

public interface UserIm {
    public User findUser(Integer id);
    public int addUser(User user);
    public int updateUser(User user);
    public int deleteUser(Integer id);
    public  List<User> allUser();
}
