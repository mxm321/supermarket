package edu.cn.controller;

import edu.cn.User;
import edu.cn.service.UserIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserIm userIm;
    //根据id查用户
    @RequestMapping(value="/user/{id}")
    @ResponseBody
    public User findUser(@PathVariable("id") Integer id){
        return userIm.findUser(id);
    }
    //删除用户
    @RequestMapping(value="/deleteuser/{id}",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String deleteUser(@PathVariable("id") Integer id){
        Integer result = userIm.deleteUser(id);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }
    }
    //添加用户
    @RequestMapping(value = "/adduser",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String addUser(@RequestBody User user){
        Integer result = userIm.addUser(user);
        if(result!=1){
            return "添加失败";
        }else {
            return "添加成功";
        }
}

    @RequestMapping(value = "/updateuser",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String updateUser(@RequestBody User user){
        Integer result = userIm.updateUser(user);
        if(result!=1){
            return "修改失败";
        }else {
            return "修改成功";
        }
    }
    //查询所有用户
    @RequestMapping(value = "/alluser")
    @ResponseBody
    public List<User> allUser(){
        return userIm.allUser();
    }
}
