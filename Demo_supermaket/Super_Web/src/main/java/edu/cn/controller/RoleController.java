package edu.cn.controller;

import edu.cn.Role;
import edu.cn.service.RoleIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

public class RoleController {
    @Autowired
    private RoleIm roleIm;
    //根据ID查角色
    @RequestMapping(value = "/role/{id}")
    @ResponseBody
    public Role findRole(@PathVariable("id") Integer id){
        return roleIm.findRole(id);
    }
    //删除角色
    @RequestMapping(value = "/roledelete/{id}",produces = "text/plain;charset=utf-8")
    @ResponseBody
    public String deleteRole(@PathVariable("id") Integer id){
        Integer result = roleIm.deleteRole(id);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }
    }
    //修改角色
    @RequestMapping(value = "/roleupdate",produces = "text/plain;charset=utf-8")
    @ResponseBody
    public String updateRole(@RequestBody Role role){
        Integer result = roleIm.updateRole(role);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }

    }
    //增加新角色
    @RequestMapping(value = "/addRole",produces = "text/plain;charset=utf-8")
    @ResponseBody
    public String addRole(@RequestBody Role role){
        Integer result = roleIm.addRole(role);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }

    }
}
