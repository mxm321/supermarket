package edu.cn.controller;

import edu.cn.Address;
import edu.cn.service.AddressIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

public class AddressController {
    @Autowired
    private AddressIm addressIm;
    //根据ID查地址
    @RequestMapping(value = "/address/{id}")
    @ResponseBody
    public Address findAddress(@PathVariable("id") Integer id){
        return addressIm.findAddress(id);
    }
    //删除地址
    @RequestMapping(value = "/addressdelete/{id}",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String deleteAddress(@PathVariable("id") Integer id){
        Integer result = addressIm.deleteAddress(id);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }
    }
    //修改地址
    @RequestMapping(value = "/addressUpdate",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String updateAddress(@RequestBody Address address){
        Integer result = addressIm.UpdateAddress(address);
        if(result!=1){
            return "修改失败";
        }else {
            return "修改成功";
        }
    }
    //增加新地址
    @RequestMapping(value = "/addAddress",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String addAddress(@RequestBody Address address){
        Integer result = addressIm.addAddress(address);
        if(result!=1){
            return "添加失败";
        }else {
            return "添加成功";
        }
    }
    //查询所有地址
    @RequestMapping(value = "/allAddress")
    @ResponseBody
    public List<Address> allAddress(){
        return addressIm.allAddress();
    }
}
