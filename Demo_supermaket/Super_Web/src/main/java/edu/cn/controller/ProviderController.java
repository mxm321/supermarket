package edu.cn.controller;

import edu.cn.Provider;
import edu.cn.service.ProviderIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

public class ProviderController {
    @Autowired
    private ProviderIm providerIm;
    //根据ID查供货商
    @RequestMapping(value = "/provider/{id}")
    @ResponseBody
    public Provider findProvider(@PathVariable("id") Integer id){
        return providerIm.findProvider(id);
    }
    //删除供货商
    @RequestMapping(value = "/providerdelete/{id}",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String deleteProvider(@PathVariable("id") Integer id){
        Integer result = providerIm.deleteProvider(id);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }
    }
    //修改产品
    @RequestMapping(value = "/providerUpdate",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String updateProvider(@RequestBody Provider provider){
        Integer result = providerIm.UpdateProvider(provider);
        if(result!=1){
            return "修改失败";
        }else {
            return "修改成功";
        }
    }
    //增加新的供货商
    @RequestMapping(value = "/addProvider",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String addProvider(@RequestBody Provider provider){
        Integer result = providerIm.addProvider(provider);
        if(result!=1){
            return "添加失败";
        }else {
            return "添加成功";
        }
    }
    //查询所有供货商
    @RequestMapping(value = "/allProvider")
    @ResponseBody
    public List<Provider> allProvider(){
        return providerIm.allProvider();
    }
}
