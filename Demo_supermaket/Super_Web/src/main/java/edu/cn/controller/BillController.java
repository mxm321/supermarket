package edu.cn.controller;

import edu.cn.Bill;
import edu.cn.service.BillIm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class BillController {
    @Autowired
    private BillIm billIm;
    //根据ID查产品
    @RequestMapping(value = "/bill/{id}")
    @ResponseBody
    public Bill findBill(@PathVariable("id") Integer id){
        return billIm.findBill(id);
    }
    //删除产品
    @RequestMapping(value = "/billdelete/{id}",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String deleteBill(@PathVariable("id") Integer id){
        Integer result = billIm.deleteBill(id);
        if(result!=1){
            return "删除失败";
        }else {
            return "删除成功";
        }
        // return billIm.deleteBill(id);
    }
    //修改产品
    @RequestMapping(value = "/billUpdate",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String updateBill(@RequestBody Bill bill){
        Integer result = billIm.UpdateBill(bill);
        if(result!=1){
            return "修改失败";
        }else {
            return "修改成功";
        }
        //return billIm.UpdateBill(bill);
    }
    //增加新的产品
    @RequestMapping(value = "/addBill",produces="text/plain;charset=utf-8")
    @ResponseBody
    public String addBill(@RequestBody Bill bill){
        Integer result = billIm.addBill(bill);
        if(result!=1){
            return "添加失败";
        }else {
            return "添加成功";
        }
    }
    //查询所有产品
    @RequestMapping(value = "/allBill")
    @ResponseBody
    public List<Bill> allBill(){
        return billIm.allBill();
    }
}
