package edu.cn.dao;

import edu.cn.Bill;

import java.util.List;

public interface BillDao {
    public Bill findBill(Integer id);
    public int addBill(Bill bill);
    public int UpdateBill(Bill bill);
    public int deleteBill(Integer id);
    public List<Bill> allBill();

}
