package edu.cn.dao;

import edu.cn.Address;

import java.util.List;

public interface AddressDao {
    public Address findAddress(Integer id);
    public int addAddress(Address address);
    public int UpdateAddress(Address address);
    public int deleteAddress(Integer id);
    public List<Address> allAddress();
}
