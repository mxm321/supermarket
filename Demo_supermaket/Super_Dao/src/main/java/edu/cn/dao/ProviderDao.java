package edu.cn.dao;

import edu.cn.Provider;

import java.util.List;

public interface ProviderDao {
    public Provider findProvider(Integer id);
    public int addProvider(Provider provider);
    public int UpdateProvider(Provider provider);
    public int deleteProvider(Integer id);
    public List<Provider> allProvider();

}
