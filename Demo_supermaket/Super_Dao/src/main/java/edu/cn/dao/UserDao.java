package edu.cn.dao;

import edu.cn.User;

import java.util.List;

public interface UserDao {
    public User findUser(Integer id);
    public int addUser(User user);
    public int updateUser(User user);
    public int deleteUser(Integer id);
    public List<User> allUser();


}
